package biblioteca.repository.repo;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.ref.WeakReference;

import static org.junit.Assert.*;

public class CartiRepoTest {

    private CartiRepo repo;
    private BibliotecaCtrl ctrl;
    private Carte c;
    @Before
    public void setUp() throws Exception{
        c= new Carte();
        repo = new CartiRepo("testFile.txt");
        ctrl = new BibliotecaCtrl(repo);
        repo.clear();

    }

    @After
    public void tearDown(){
        WeakReference<CartiRepo> wc = new WeakReference<CartiRepo>(repo);
        WeakReference<BibliotecaCtrl> wb = new WeakReference<BibliotecaCtrl>(ctrl);
        WeakReference<Carte> wo = new WeakReference<Carte>(c);
        repo = null;
        ctrl = null;
        c = null;
        while(wc.get()!=null && wo.get()!=null && wb.get()!=null)
            System.gc();
    }

    @Test
    public void TC1_EC() throws Exception {
        c.setTitlu("carte");
        c.adaugaReferent("autorA");
        c.adaugaReferent("autorB");
        c.adaugaReferent("autorC");
        c.setAnAparitie("1854");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        assertEquals(1,repo.getCarti().size());
    }

    @Test
    public void TC2_EC() throws Exception {
        c.setTitlu("carte");
        c.setAnAparitie("1854");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        try{
            ctrl.adaugaCarte(c);
            assert (false);
        }catch (Exception e){
            assert(true);
        }

        assertEquals(0,repo.getCarti().size());
    }

    @Test
    public void TC3_EC() throws Exception {
        c.setTitlu("");
        c.adaugaReferent("autorA");
        c.adaugaReferent("autorB");
        c.adaugaReferent("autorC");
        c.setAnAparitie("1854");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        try {
            ctrl.adaugaCarte(c);
            assert (false);
        }catch (Exception e){
            assert (true);
        }
        assertEquals(0,repo.getCarti().size());
    }

    @Test
    public void TC4_EC() throws Exception {
        c.setTitlu("carte");
        c.adaugaReferent("autorA");
        c.adaugaReferent("autorB");
        c.adaugaReferent("autorC");
        c.setAnAparitie("asdf");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        try{
            ctrl.adaugaCarte(c);
            assert (false);
        }catch (Exception e){
            assert (true);
        }
        assertEquals(0,repo.getCarti().size());
    }

    @Test
    public void TC6_EC() throws Exception {
        c.setTitlu("carte");
        c.adaugaReferent("autorA");
        c.adaugaReferent("autorB");
        c.adaugaReferent("autorC");
        c.setAnAparitie("1854");
        try {
            ctrl.adaugaCarte(c);
            assert (false);
        }catch (Exception e){
            assert (true);
        }
        assertEquals(0,repo.getCarti().size());
    }

    @Test
    public void TC3_BVA() throws Exception {
        c.setTitlu("M");
        c.adaugaReferent("autorA");
        c.adaugaReferent("autorB");
        c.setAnAparitie("1");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        assertEquals(1,repo.getCarti().size());
    }

    @Test
    public void TC13_BVA() throws Exception {
        c.setTitlu("carte");
        c.adaugaReferent("autorA");
        c.adaugaReferent("autorB");
        c.setAnAparitie("-1");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        try {
            ctrl.adaugaCarte(c);
            assert (false);
        }catch (Exception e){
            assert (true);
        }
        assertEquals(0,repo.getCarti().size());
    }

    @Test
    public void TC4_BVA() throws Exception {
        long maxSize = Runtime.getRuntime().totalMemory();
        int titleSize = (int) (maxSize/2);
        char[] titlu = new char[titleSize];
        for(int i=0;i<titlu.length;i++){
            titlu[i]='a';
        }
        c.setTitlu(new String(titlu));
        c.adaugaReferent("autorA");
        c.adaugaReferent("autorB");
        c.setAnAparitie("1");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        assertEquals(1,repo.getCarti().size());
    }

    @Test
    public void TC5_BVA() throws Exception {
        long maxSize = Runtime.getRuntime().totalMemory();
        int titleSize = (int) (maxSize/2);
        //char[] titlu = new char[titleSize-1];
        char[] titlu = new char[10];
        for(int i=0;i<titlu.length;i++){
            titlu[i]='a';
        }
        c.setTitlu(new String(titlu));
        c.adaugaReferent("autorA");
        c.adaugaReferent("autorB");
        c.setAnAparitie("1");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        assertEquals(1,repo.getCarti().size());
    }

    @Test
    public void TC14_BVA() throws Exception {
        try {
            long maxSize = Runtime.getRuntime().totalMemory();
            int anSize = (int) (maxSize / 2);
            char[] an = new char[anSize + 1];
            for (int i = 0; i < an.length; i++) {
                an[i] = '1';
            }
            c.setTitlu("title");
            c.adaugaReferent("autorA");
            c.adaugaReferent("autorB");
            //c.setAnAparitie(new String(an));
            c.adaugaCuvantCheie("carte");
            c.adaugaCuvantCheie("editura");

            ctrl.adaugaCarte(c);
            assert (false);
        }catch (Exception e){
            assert(true);
        }
        assertEquals(0,repo.getCarti().size());
    }

    @Test
    public void Tc01_WBT() throws Exception {
        assertEquals(ctrl.cautaCarte("autor").size(),0);
    }

    @Test
    public void TC03_WBT() throws Exception{
        c.setTitlu("carte");
        c.adaugaReferent("aA");
        c.adaugaReferent("aurB");
        c.adaugaReferent("aurC");
        c.setAnAparitie("1854");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        assertEquals(ctrl.cautaCarte("autor").size(),0);
    }

    @Test
    public void TC04_WBT() throws Exception{
        c.setTitlu("carte");
        c.adaugaReferent("autorA");
        c.adaugaReferent("aurgrB");
        c.adaugaReferent("aurC");
        c.setAnAparitie("1854");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        assertEquals(ctrl.cautaCarte("autor").size(),1);
    }

}
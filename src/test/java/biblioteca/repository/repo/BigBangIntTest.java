package biblioteca.repository.repo;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.ref.WeakReference;

import static org.junit.Assert.*;


public class BigBangIntTest {

    private CartiRepo repo;
    private BibliotecaCtrl ctrl;
    private Carte c;

    @Before
    public void setUp() throws Exception{
        c= new Carte();
        repo = new CartiRepo("testFile.txt");
        ctrl = new BibliotecaCtrl(repo);
        repo.clear();

    }

    @After
    public void tearDown(){
        WeakReference<CartiRepo> wc = new WeakReference<CartiRepo>(repo);
        WeakReference<BibliotecaCtrl> wb = new WeakReference<BibliotecaCtrl>(ctrl);
        WeakReference<Carte> wo = new WeakReference<Carte>(c);
        repo = null;
        ctrl = null;
        c = null;
        while(wc.get()!=null && wo.get()!=null && wb.get()!=null)
            System.gc();
    }

    @Test
    public void TC1_EC() throws Exception {
        c.setTitlu("carte");
        c.adaugaReferent("autorA");
        c.adaugaReferent("autorB");
        c.adaugaReferent("autorC");
        c.setAnAparitie("1854");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        assertEquals(1,repo.getCarti().size());
    }

    @Test
    public void TC04_WBT() throws Exception{
        c.setTitlu("carte");
        c.adaugaReferent("autorA");
        c.adaugaReferent("aurgrB");
        c.adaugaReferent("aurC");
        c.setAnAparitie("1854");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        assertEquals(ctrl.cautaCarte("autor").size(),1);
    }

    @Test
    public void TC1_Cautare() throws Exception{
        c.setTitlu("carte");
        c.adaugaReferent("autorA");
        c.adaugaReferent("aurgrB");
        c.adaugaReferent("aurC");
        c.setAnAparitie("1854");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        try{
            int size = ctrl.getCartiOrdonateDinAnul("asdf").size();
            assert (false);
            assertEquals(size,0);
        }catch (Exception e){
            if(e.getMessage().equals("Nu e numar!"))
                assert (true);
            else
                assert (false);
        }
    }

    @Test
    public void TC1_Int() throws Exception {
        c.setTitlu("carte");
        c.adaugaReferent("autorA");
        c.adaugaReferent("aurgrB");
        c.adaugaReferent("aurC");
        c.setAnAparitie("1854");
        c.adaugaCuvantCheie("carte");
        c.adaugaCuvantCheie("editura");
        ctrl.adaugaCarte(c);
        c.setAnAparitie("1920");
        c.setTitlu("cart");
        ctrl.adaugaCarte(c);
        assertEquals(ctrl.cautaCarte("autorA").size(),2);
        assertEquals(ctrl.getCartiOrdonateDinAnul("1854").size(),1);
    }


}
